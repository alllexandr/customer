package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/streadway/amqp"
)

type status struct {
	VehicleID	string		`json:"vehicle_id"`
	Connected bool 		`json:"connected"`
	Timestamp  int64 	`json:"timestamp"`
}

var (
	customerID = mustGetenv("CUSTOMER_ID")
	vehicleStatus status
)

func RandBool() bool {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(2) == 1
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func mustGetenv(k string) string {
	v := os.Getenv(k)
	if v == "" {
		log.Fatalf("%s environment variable not set.", k)
	}
	return v
}

func RedinessProbe(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Im ready ))), %s!", r.URL.Path[1:])
}

func main() {

	http.HandleFunc("/ready", RedinessProbe)
	go http.ListenAndServe(":3000", nil)

	conn, err := amqp.Dial( mustGetenv("RABBITMQ_CON_STRING"))
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		fmt.Sprintf("entity_%s",customerID), // name
		true,         // durable
		false,        // delete when unused
		false,        // exclusive
		false,        // no-wait
		nil,          // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	failOnError(err, "Failed to set QoS")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {

			err := json.Unmarshal(d.Body,&vehicleStatus)
			if err != nil {
				log.Print(err)
				return
			}
			log.Printf("%d: Vehicle [%s] connected [%t]", vehicleStatus.Timestamp,vehicleStatus.VehicleID,vehicleStatus.Connected)
			dot_count := bytes.Count(d.Body, []byte("."))
			t := time.Duration(dot_count)
			time.Sleep(t * time.Second)
			log.Printf("Done")
			d.Ack(false)
		}
	}()

	log.Printf("CustormerID [%s] started",customerID)


	<-forever
}